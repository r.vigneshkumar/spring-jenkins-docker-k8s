package com.dockersample.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@GetMapping("/")
	public String getEmpty() {
		
		return "Welcome Pavii jenkins deploy K8S";
	}
}
